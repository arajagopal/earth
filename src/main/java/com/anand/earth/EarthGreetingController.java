package com.anand.earth;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
public class EarthGreetingController {
    private static final String template = "Hello from Spring, %s!";

    private RestTemplate restTemplate = new RestTemplate();

    @RequestMapping(value = "/HelloWorld_war/{name}", method = RequestMethod.GET)
    public String greeting(@PathVariable(value = "name") String name) {
        String count_service = System.getenv("count_service");
        System.out.println("http://" + count_service + "/increment/earth");
        restTemplate.put("http://" + count_service + "/increment/{name}", null, "earth");
        return String.format(template, name);
    }
}