FROM openjdk:8-jre-alpine
COPY ./build/libs/earth-0.0.1-SNAPSHOT.jar /usr/src/greeting/
WORKDIR /usr/src/greeting
EXPOSE 8080
CMD ["java", "-XX:+PrintGCDetails", "-Xloggc:/etc/gc/gclog", "-jar", "earth-0.0.1-SNAPSHOT.jar"]